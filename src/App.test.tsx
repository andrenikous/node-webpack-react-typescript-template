import {fireEvent, render, screen} from '@testing-library/react';
import React from 'react';
import App from './App';

describe('Testing App Component', () => {
  it('should show Hello React when first loaded', () => {
    render(<App />);
    expect(screen.getByText('Hello React')).toBeDefined;
  });
  it('should show Goodbye React after clicking button', () => {
    render(<App />);
    fireEvent(screen.getByText('Goodbye'), new MouseEvent('click', {
      bubbles: true,
      cancelable: true,
    }));
    expect(screen.getByText('Goodbye React')).toBeDefined;
  });
});
