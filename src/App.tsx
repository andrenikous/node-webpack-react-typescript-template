import React, {useState} from 'react';

const App: React.FC = () => {
  const [statement, setStatement] = useState('Hello');

  return (
    <div>
      <h1>{`${statement}`} React</h1>
      <button onClick={() => {
        setStatement('Goodbye');
      }} >Goodbye</button>
    </div>
  );
};

export default App;
